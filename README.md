# README #

The repository was created to document work done for an interview homework exercise.
The source code is made up of two parts:

1. The packages in com.brein.time are an implemention of an 
[Interval Tree](https://en.wikipedia.org/wiki/Interval_tree#Java_example:_Adding_a_new_interval_to_the_tree) retrieved 
from [Breinify](https://github.com/Breinify/brein-time-utilities/#intervaltree). This code has few viewer components 
removed to prevent build dependency issues.

2. The single class ZipRangeMain in the package com.zipsrus and the unit tests in ZipRangeMainTest were written by me 
to demonstrate how to use an IntervalTree to reduce a set of zip code intervals to a minimal set. 

### How do I get set up? ###

This repository can built off of the master branch using maven (i.e. "mvn package" or "mvn test") either on the command 
line or through something like IntelliJ's junit test viewer.


### Who do I talk to? ###

* This repository was created by Curtis Crum who can be reached at calypso1999@gmail.com.