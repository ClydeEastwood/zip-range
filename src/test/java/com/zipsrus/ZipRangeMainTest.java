package com.zipsrus;

import com.brein.time.timeintervals.indexes.IntervalTree;
import com.brein.time.timeintervals.intervals.Interval;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static org.junit.Assert.assertTrue;

/**
 * This is the main test class for the project. Test include:
 * 1. Verify the (2) provided test cases are handled. These exercise the contiguous cases where intervals need to be
 * merged and one form of overlap is addressed.
 * 2. Verify and additional case of overlap where a single new interval overlaps multiple existing intervals.
 */
public class ZipRangeMainTest {
    static ZipRangeMain zipRangeMain;
    static Collection<Interval> intervalsIn;
    @Before
    public void setUp() throws Exception {
        zipRangeMain = new ZipRangeMain();
        intervalsIn = new ArrayList();
    }

    /**
     If the input = [94133,94133] [94200,94299] [94600,94699]
     Then the output should be = [94133,94133] [94200,94299] [94600,94699]
     * @throws Exception
     */
    @Test
    public void testCanGetSampleResults1() throws Exception {
        intervalsIn.add(new Interval(94133,94133));
        intervalsIn.add(new Interval(94200,94299));
        intervalsIn.add(new Interval(94600,94699));
        Collection<Interval> intervalsOut = zipRangeMain.findMinimalRangeSets(intervalsIn);
        assertTrue(intervalsOut.size() == 3);

        Iterator<Interval> iterator = intervalsOut.iterator();
        Interval interval1 = iterator.next();
        assertTrue(interval1.getStart() == 94133 && interval1.getEnd() == 94133);

        Interval interval2 = iterator.next();
        assertTrue(interval2.getStart() == 94200 && interval2.getEnd() == 94299);

        Interval interval3 = iterator.next();
        assertTrue(interval3.getStart() == 94600 && interval3.getEnd() == 94699);
    }

    /**
     If the input = [94133,94133] [94200,94299] [94226,94399]
     Then the output should be = [94133,94133] [94200,94399]
     * @throws Exception
     */
    @Test
    public void testCanGetSampleResults2() throws Exception {
        intervalsIn.add(new Interval(94133,94133));
        intervalsIn.add(new Interval(94200,94299));
        intervalsIn.add(new Interval(94226,94399));
        Collection<Interval> intervalsOut = zipRangeMain.findMinimalRangeSets(intervalsIn);
        assertTrue(intervalsOut.size() == 2);

        Iterator<Interval> iterator = intervalsOut.iterator();
        Interval interval1 = iterator.next();
        assertTrue(interval1.getStart() == 94133 && interval1.getEnd() == 94133);

        Interval interval2 = iterator.next();
        assertTrue(interval2.getStart() == 94200 && interval2.getEnd() == 94399);
    }

    /**
     * When intervals cannot be reduced.
     * @throws Exception
     */
    @Test
    public void testCannotReduce() throws Exception {
        intervalsIn.add(new Interval(1,2));
        intervalsIn.add(new Interval(4,5));
        Collection<Interval> intervalsOut = zipRangeMain.findMinimalRangeSets(intervalsIn);
        assertTrue(intervalsOut.size() == 2);
    }

    /**
     * Simplified reduce for debugging purposes.
     * @throws Exception
     */
    @Test
    public void testCanReduce1() throws Exception {
        intervalsIn.add(new Interval(1,3));
        intervalsIn.add(new Interval(2,4));
        Collection<Interval> intervalsOut = zipRangeMain.findMinimalRangeSets(intervalsIn);
        assertTrue(intervalsOut.size() == 1);
    }

    /**
     * Multiple contiguous intervals overlapped by a single new interval.
     * @throws Exception
     */
    @Test
    public void testCanReduce2() throws Exception {
        intervalsIn.add(new Interval(1,1));
        intervalsIn.add(new Interval(2,3));
        intervalsIn.add(new Interval(3,4));
        intervalsIn.add(new Interval(1,7));
        Collection<Interval> intervalsOut = zipRangeMain.findMinimalRangeSets(intervalsIn);
        Interval i = intervalsOut.iterator().next();
        assertTrue(intervalsOut.size() == 1);
        assertTrue(i.getStart() == 1);
        assertTrue(i.getEnd() == 7);
    }

    /**
     * Verify interval tree sorts intervals in its iterator.
     */
    @Test
    public void testCountOnOrderedInterator() throws Exception {
        intervalsIn.add(new Interval(4,5));
        intervalsIn.add(new Interval(7,8));
        intervalsIn.add(new Interval(1,2));
        IntervalTree intervalTree = new IntervalTree();
        intervalTree.addAll(intervalsIn);
        Iterator<Interval> iterator = intervalTree.iterator();
        assertTrue(iterator.next().getStart() == 1);
        assertTrue(iterator.next().getStart() == 4);
        assertTrue(iterator.next().getStart() == 7);
    }
}