package com.zipsrus;

import com.brein.time.timeintervals.indexes.IntervalTree;
import com.brein.time.timeintervals.intervals.Interval;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 BACKGROUND
 Sometimes items cannot be shipped to certain zip codes, and the rules for these restrictions are stored as a series of ranges of 5 digit codes. For example if the ranges are:

 [94133,94133] [94200,94299] [94600,94699]

 Then the item can be shipped to zip code 94199, 94300, and 65532, but cannot be shipped to 94133, 94650, 94230, 94600, or 94299.

 Any item might be restricted based on multiple sets of these ranges obtained from multiple sources.

 PROBLEM
 Given a collection of 5-digit ZIP code ranges (each range includes both their upper and lower bounds), provide an algorithm that produces the minimum number of ranges required to represent the same restrictions as the input.

 NOTES
 - The ranges above are just examples, your implementation should work for any set of arbitrary ranges
 - Ranges may be provided in arbitrary order
 - Ranges may or may not overlap
 - Your solution will be evaluated on the correctness and the approach taken, and adherence to coding standards and best practices

 EXAMPLES:
 If the input = [94133,94133] [94200,94299] [94600,94699]
 Then the output should be = [94133,94133] [94200,94299] [94600,94699]

 If the input = [94133,94133] [94200,94299] [94226,94399]
 Then the output should be = [94133,94133] [94200,94399]

 Evaluation Guidelines:
 Your work will be evaluated against the following criteria:
 - Successful implementation
 - Efficiency of the implementation
 - Design choices and overall code organization
 - Code quality and best practices
 */
public class ZipRangeMain {

    private static Logger log = Logger.getLogger(com.zipsrus.ZipRangeMain.class);

    /**
     * Used to verify simple run through main.
     * @param args
     */
    public static void main(String[] args) {
        BasicConfigurator.configure();
        log.info("Reducing zip code ranges...");
        ZipRangeMain zipRangeMain = new ZipRangeMain();
        Collection<Interval> intervalsIn = new ArrayList();
        intervalsIn.add(new Interval(1,3));
        intervalsIn.add(new Interval(2,4));
        zipRangeMain.printZipRangeSets(intervalsIn);
        Collection<Interval> intervalsOut = zipRangeMain.findMinimalRangeSets(intervalsIn);
        zipRangeMain.printZipRangeSets(intervalsOut);
    }

    // assumes these intervals do intersect
    private Interval mergeIntervals(Interval interval1, Interval interval2) {
        long begin;
        long end;
        if (interval1.getStart() < interval2.getStart()) {
            begin = interval1.getStart();
        } else {
            begin = interval2.getStart();
        }
        if (interval1.getEnd() > interval2.getEnd()) {
            end = interval1.getEnd();
        } else {
            end = interval2.getEnd();
        }
        return new Interval(begin,end);
    }

    /**
     * <API entry point>
     * This method uses an IntervalTree with overlap removal and contiguous region merging to reduce a set of input
     * intervals to a minimal set without overlapping or touching intervals.
     * @param intervalsIn a Collection of Interval items.
     * @return a reduced Collection of Interval items.
     */
    public Collection<Interval> findMinimalRangeSets(Collection<Interval> intervalsIn) {
        IntervalTree tree = new IntervalTree();

        for (Interval interval:intervalsIn) {
            Collection<Interval> intervals = tree.overlap(interval);
            // the case where a single new interval overlaps multiple existing intervals
            if (intervals.size() > 1) {
                long newBeginning = -1;
                long newEnding = -1;
                for (Interval i: intervals) {
                    // Inside intervals are removed
                    if (i.getStart() >= interval.getStart() && i.getEnd() <= interval.getEnd()) {
                        tree.remove(i);
                        newBeginning = interval.getStart();
                        newEnding = interval.getEnd();

                    // An interval overlapping the beginning contributes to the new beginning
                    } else if (i.getStart() < interval.getStart()) {
                        newBeginning = i.getStart();

                    // An interval overlapping the end contributes to the new ending
                    } else if (i.getEnd() > interval.getEnd()) {
                        newBeginning = i.getEnd();
                    }
                }
                if (newBeginning != -1 || newEnding != -1) {
                    if (newBeginning == -1) {
                        newBeginning = interval.getStart();
                    }
                    if (newEnding == -1) {
                        newEnding = interval.getEnd();
                    }
                    tree.remove(interval);
                    tree.add(new Interval(newBeginning,newEnding));
                }
            } else if (intervals.size() == 1) {
                Interval existingInterval = intervals.iterator().next();
                Interval mergedInterval = mergeIntervals(interval,existingInterval);
                tree.remove(existingInterval);
                tree.add(mergedInterval);
            } else {
                tree.add(interval);
            }
        }
        return mergeContiguousIntervals(tree);
    }

    // assumes iterator returns a sorted list non-overlapping Intervals
    private IntervalTree mergeContiguousIntervals(IntervalTree tree) {
        Interval firstInterval = null;
        Interval secondInterval = null;
        Interval newInterval = null;
        boolean endReached = false;
        while (!endReached) {
            boolean foundContiguous = false;
            Iterator<Interval> iterator = tree.iterator();
            while (iterator.hasNext()) {
                firstInterval = iterator.next();
                if (iterator.hasNext()) {
                    secondInterval = iterator.next();
                    if (!iterator.hasNext()) {
                        endReached = true;
                    }
                    if (firstInterval.getEnd() + 1 == secondInterval.getStart()) {
                        foundContiguous = true;
                        newInterval = new Interval(firstInterval.getStart(), secondInterval.getEnd());
                        break;
                    }
                } else {
                    endReached = true;
                }
            }
            if (foundContiguous) {
                tree.remove(firstInterval);
                tree.remove(secondInterval);
                tree.add(newInterval);
            }
        }
        return tree;
    }


    public void printZipRangeSets(Collection<Interval> zipRanges) {
        if (zipRanges == null) {
            log.warn("Null zip range.");
        } else {
            for (Interval interval : zipRanges) {
                log.info(interval);
            }
        }
    }
}
